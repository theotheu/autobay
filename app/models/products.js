/**
 * Module dependencies.
 */
var mongoose;

mongoose = require('mongoose'),
    Schema = mongoose.Schema;

// Subdocuments @ http://mongoosejs.com/docs/subdocs.html
/* Sub Schema definitions */
// Joins @ http://mongoosejs.com/docs/populate.html
var carSchemaName = Schema({
    make: {type: String, required: true},
    style: {type: String, required: true},
    fuel: {type: String, required: true},
    engine: {type: Number, required: true},
    power: {type: Number, required: true},
    year: {type: Number, required: true},
    color: {type: String, required: true},
    imageUrl: {type: String, required: true}
});

/* Schema definitions */
var schemaName = Schema({
    productId: {type: String, required: true, unique: true},
    name: {type: String, required: true},
    price: {type: String, required: true},
    cars: [carSchemaName],
    modificationDate: {type: Date, "default": Date.now}
});

/*
 If collectionName is absent as third argument, than the modelName should always end with an -s.
 Mongoose pluralizes the model name. (This is not documented)
 */
var modelName = "Product";
var collectionName = "products"; // Naming convention is plural.
mongoose.model(modelName, schemaName, collectionName);