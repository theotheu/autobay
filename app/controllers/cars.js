var mongoose = require('mongoose')
    , Car = mongoose.model('Car');


// CREATE
// save @ http://mongoosejs.com/docs/api.html#model_Model-save
exports.create = function (req, res) {

    var doc = new Car(req.body);

    doc.save(function (err) {
        var retObj = {
            meta: {"action": "create", "timestamp": new Date()},
            doc: doc,
            err: err
        };
        return res.send(retObj);
    });

}

// RETRIEVE
// find @ http://mongoosejs.com/docs/api.html#model_Model.find
exports.list = function (req, res) {
    var conditions, fields, options;

    conditions = {isSold: false};
    fields = {};
    options = {'createdAt': -1};

    Car
        .find(conditions, fields, options)
        .exec(function (err, doc) {
            var retObj = {
                meta: {"action": "list", "timestamp": new Date(), "conditions": conditions},
                doc: doc,
                err: err
            };
            return res.send(retObj);
        })
}

exports.detail = function (req, res) {
    var conditions, fields, options;

    conditions = {"id": parseInt(req.params.id)}  // make sure that the given parameter is passed as a number
        , fields = {}
        , options = {'createdAt': -1};

    Car
        .find(conditions, fields, options)
        .exec(function (err, doc) {
            var retObj = {
                meta: {"action": "detail", "timestamp": new Date(), "conditions": conditions},
                doc: doc[0],
                err: err
            };
            console.log(retObj);
            return res.send(retObj);
        })
}

// UPDATE
// findOneAndUpdate @ http://mongoosejs.com/docs/api.html#model_Model.findOneAndUpdate
exports.update = function (req, res) {

    var conditions =
        {email: req.params.email}
        , update = {
            name: req.body.name}
        , options = { multi: true }
        , callback = function (err, doc) {
            var retObj = doc;
            return res.send(retObj);
        }

    Car.findOneAndUpdate(conditions, update, options, callback);
}

// DELETE
// remove @ http://mongoosejs.com/docs/api.html#model_Model-remove
exports.delete = function (req, res) {
    var conditions, callback;

    conditions = {_id: req.params.id}
        , callback = function (err, doc) {
        var retObj = doc;
        return res.send(retObj);
    }

    Car.remove(conditions, callback);
}