var mongoose = require('mongoose')
    , Transaction = mongoose.model('Transaction')
    , Relation = mongoose.model('Relation')
    , Car = mongoose.model('Car')
    , passwordHash = require('password-hash');


// CREATE


// save @ http://mongoosejs.com/docs/api.html#model_Model-save


function saveTransaction(transaction, cb) {
    var doc;

    doc = new Transaction(transaction);
    doc.save(function (err) {
        var retObj = {
            meta: {"action": "create transaction", 'timestamp': new Date()},
            doc: doc,
            err: err
        };
        console.log(retObj);
        return cb.send(retObj);
    });


}

function findOrCreateUser(name, transaction, cb) {
    var conditions, fields, options;

    conditions = {name: name}
        , fields = {}
        , options = {'createdAt': -1};

    Relation
        .find(conditions, fields, options)
        .exec(function (err, doc) {
            if (doc.length === 0) {
                doc = new Relation({
                    name: name,
                    email: name.replace(/[\W|\s]/g, '').toLowerCase() + "@tezzt.nl",
                    password: passwordHash.generate("TopSecret!")
                })
                doc.save(function (err) {
                    transaction._relation = doc._id;
                    saveTransaction(transaction, cb);

                });
            } else {
                transaction._relation = doc[0]._id;
                saveTransaction(transaction, cb);
            }
        })
}


exports.create = function (req, res) {
    var name, transaction;

    name = req.body.name;

    transaction = {
        _car: req.body.carId,
        bid: parseFloat(req.body.bid || 0)
    };
    findOrCreateUser(name, transaction, res);


    return;
    // Encrypt password


    var doc = new Transaction(transaction);

    findOrCreateUser(doc, res)

    console.log('----------');
    console.log(doc);
    console.log(req.body);


    doc.save(function (err) {
        var retObj = {
            meta: {"action": "create", 'timestamp': new Date()},
            doc: doc,
            err: err
        };

        console.log('---------- OUT');
        console.log(doc);
        console.log('^^^^^^^^^^ OUT');


        return res.send(retObj);
    });

}

// RETRIEVE
// find @ http://mongoosejs.com/docs/api.html#model_Model.find
exports.list = function (req, res) {
    var conditions, fields, options;

    conditions = {};
    fields = {};
    options = {'createdAt': -1};

    Transaction
        .find(conditions, fields, options)
        .populate("_car _relation")
        .exec(function (err, doc) {
            var retObj = {
                meta: {"action": "list", 'timestamp': new Date()},
                doc: doc,
                err: err
            };
            return res.send(retObj);
        })
}


exports.detail = function (req, res) {
    var conditions, fields, options;

    console.log(req.params.id);

    conditions = {_id: req.params.id}
        , fields = {}
        , options = {'createdAt': -1};

    Transaction
        .findOne(conditions, fields, options)
        .populate("_relation")
        //.sort({'createdAt': -1})// sort by date
        .exec(function (err, doc) {
            //if (err) return handleError(err);
            console.log('vvvvvvvvvvvvvvvv detail');
            console.log(doc);
            console.log('^^^^^^^^^^^^^^^^ detail');


            var retObj = {
                meta: {"action": "detail", 'timestamp': new Date()},
                doc: doc,
                err: err
            };
            return res.send(retObj);
        })
}


exports.bidsPerCar = function (req, res) {
    var conditions, fields, options, o, mapFunction, reduceFunction, retObj;
    console.log("bidsPerCar");

    o = {};

    o.mapFunction = function () {
        emit(this._relation, this.bid);
    };

    o.reduceFunction = function (relationId, valuesBid) {
        return Array.sum(valuesBid);
    };

    console.log(o);


//    Transaction
//        .mapReduce(o, function (err, docs) {
//            retObj = {
//                meta: {"action": "bidsPerCar", 'timestamp': new Date(), mapFunction: mapFunction
//                }
//            };
//            return res.send(retObj);
//        });


    /*
     sql examples
     @see http://architects.dzone.com/articles/comparing-mongodb-new
     @see http://docs.mongodb.org/manual/reference/sql-comparison/
     @see http://docs.mongodb.org/manual/reference/sql-aggregation-comparison/
     @see http://nosql.mypopescu.com/post/392418792/translate-sql-to-mongodb-mapreduce (IMAGE)
     */

//    Transaction
//        .aggregate(
//        { $group: {
//            _id: {_car: "$_car", bid:"$bid"}
//            , sumBid: {$sum: 1}
////            , avgBid: {$avg: "$bid"}
////            , maxBid: {$max: "$bid"}
//        }}
//        , {$sort: {maxBid: 1}}
//        , function (err, doc) {
//            retObj = {
//                meta: {"action": "counts total records in collection", 'timestamp': new Date(),
//                    doc: doc,
//                    err: err
//                }
//            };
//            return res.send(retObj);
//        }
//    );


    var o = {};
    o.map = function () {
        emit("_id", {
            _car: this._car,
            relationId: this._relation,
            bid: this.bid
        });
    };
    o.reduce = function (k, vals) {
        var carsAr = [];
        var car = [];
        for (var i = 0; i < vals.length; i++) {
            var _car = vals[i]._car;
            var bid = vals[i].bid;
        }
        return {vals: vals};
    };
    o.out = {replace: "createdCollectionNameForResults"};
    o.verbose = true;
    o.query = {
        _car: "51507e41a41b7ad95c059f10"
    };
    Transaction
        .mapReduce(o, function (err, model, stats) {
//            retObj = {
//                meta: {"action": "counts total records in collection", 'timestamp': new Date(), stats: stats },
//                model: model,
//                err: err
//            };
//            return res.send(retObj);


            model
                .find()
                .populate({path:"_car", model:"Car"})
                .sort({bid: -1})
                .exec(function (err, doc) {
                    retObj = {
                        meta: {"action": "counts total records in collection", 'timestamp': new Date(), stats: stats },
                        doc: doc,
                        err: err
                    };
                    return res.send(retObj);
                });


        }
    )
    ;

}
;

// UPDATE
// findOneAndUpdate @ http://mongoosejs.com/docs/api.html#model_Model.findOneAndUpdate
exports.update = function (req, res) {

    var conditions =
        {email: req.params.email}
        , update = {
            name: req.body.name}
        , options = { multi: true }
        , callback = function (err, doc) {
            var retObj = {
                meta: {"action": "update", 'timestamp': new Date()},
                doc: doc,
                err: err
            };
            return res.send(retObj);
        }

    Transaction.findOneAndUpdate(conditions, update, options, callback);
}

// DELETE
// remove @ http://mongoosejs.com/docs/api.html#model_Model-remove
exports.delete = function (req, res) {
    var conditions, callback;

    conditions = {_id: req.params.id}
        , callback = function (err, doc) {
        var retObj = {
            meta: {"action": "delete", 'timestamp': new Date()},
            doc: doc,
            err: err
        };
        return res.send(retObj);
    }

    Transaction.remove(conditions, callback);
}