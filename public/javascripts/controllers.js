//angular.module('myApp.controllers', []).
//    directive('appVersion', ['version', function (version) {
//        console.log("controllers");
//        return function (scope, elm, attrs) {
//            elm.text(version);
//        };
//    }]);


function CarListCtrl($scope, db) {
    "use strict";

    $scope.orderProp = 'make';

    $scope.cars = db.cars.get();

    $scope.remove = function (db) {
        db.$remove();
    };
}

function CarDetailCtrl($scope, $routeParams, db) {
    "use strict";
    $scope.car = db.cars.get({id: $routeParams.id});
}

function CarNewCtrl($scope, $http) {
    console.log('CarNewCtrl');
}


function TransactionNewCtrl($scope, $routeParams, $location, Transaction) {
    "use strict";
    $scope.carId = $routeParams.carId;
    $scope.price = $routeParams.price;
    $scope.minPrice = parseFloat($routeParams.price) * 0.8;
    $scope.err = ""; // Initialize err as empty string. We start with no errors.

    $scope.save = function () {
        Transaction.save({}, $scope.transaction, function (res) {
            if (res.err === null) {
                $location.path("/cars");
            } else {
                $scope.err = res.err.err;
            }
        });
    };

    $scope.getInitialValueForCarId = function () {
        return $routeParams.carId;
    };
}

function CarAggreCtrl($scope, $http) {
    "use strict";
    console.log('CarNewCtrl');
}


function CarEditCtrl($scope, $http) {
    "use strict";
    console.log('CarNewCtrl');
}


// Sharing data between controllers with a service
function MyCtrl(UserServiceThatWillBeProcessedWithAController, $scope) {
    "use strict";
    $scope.name = UserServiceThatWillBeProcessedWithAController.name;
}
