/* https://github.com/dalcib/angular-phonecat-mongodb-rest */

var res
angular.module('myApp.services', ['ngResource'])
    .factory('Transaction', ['$resource', '$http',
        function ($resource, $http) {
            var actions = {
                'get': {method: 'GET'},
                'save': {method: 'POST'},
                'query': {method: 'GET', isArray: true},
                'remove': {method: 'DELETE'},
                'delete': {method: 'DELETE'}
            };
            res = $resource('transaction/:carId', {}, actions);
            return res;

        }
    ])
    .factory('db', ['$resource', '$http',
        function ($resource, $http) {

            var actions = {
                'get': {method: 'GET'},
                'save': {method: 'POST'},
                'query': {method: 'GET', isArray: true},
                'remove': {method: 'DELETE'},
                'delete': {method: 'DELETE'}
            };
            var db = {};
            db.cars = $resource('/cars/:id', {}, actions);
            return db;
        }

    ]).factory('UserServiceThatWillBeProcessedWithAController', function () {
        return {
            name: 'CRIA-WT 2013'
        };
    });

