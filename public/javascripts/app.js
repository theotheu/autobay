/*global angular, CarListCtrl, CarNewCtrl, CarAggreCtrl, CarDetailCtrl, CarEditCtrl, TransactionNewCtrl */
(function () {
    'use strict';

// Declare app level module which depends on filters, and services
    angular.module('myApp', [ 'myApp.filters', 'myApp.services', 'myApp.directives']).
        config(['$routeProvider', function ($routeProvider) {
            // Cars
            $routeProvider.when('/cars', {templateUrl: 'partials/car-list.html', controller: CarListCtrl});
            $routeProvider.when('/car/new', {templateUrl: 'partials/car-form.html', controller: CarNewCtrl, reloadOnSearch: false});
            $routeProvider.when('/cars/aggregation', {templateUrl: 'partials/car-aggre.html', controller: CarAggreCtrl, reloadOnSearch: false});
            $routeProvider.when('/cars/:id', {templateUrl: 'partials/car-detail.html', controller: CarDetailCtrl});
            $routeProvider.when('/car/edit/:id', {templateUrl: 'partials/car-form.html', controller: CarEditCtrl, reloadOnSearch: false});
            $routeProvider.otherwise({redirectTo: '/cars', reloadOnSearch: false});
            // Transactions
            $routeProvider.when('/transactions/new/:carId/:price', {templateUrl: 'partials/transaction-form.html', controller: TransactionNewCtrl, reloadOnSearch: false});

        }]);
}())


