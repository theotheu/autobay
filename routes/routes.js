module.exports = function (app) {

    /*  Relation routes
     ---------------
     We create a variable "relations" that holds the controller object.
     We map the URL to a method in the created variable "relations".
     In this example is a mapping for every CRUD action.
     */
    var relations = require('../app/controllers/relations.js');
    // CREATE
    app.post('/relation/', relations.create);
    // RETRIEVE
    app.get('/relations', relations.list);
    app.get('/relation/:email', relations.detail);
    // UPDATE
    app.put('/relation/:email', relations.update);
    // DELETE
    app.delete('/relation/:email', relations.delete);


    /*  Product routes
     ---------------
     We create a variable "relations" that holds the controller object.
     We map the URL to a method in the created variable "relations".
     In this example is a mapping for every CRUD action.
     */
    var products = require('../app/controllers/products.js');
    // CREATE
    app.post('/product/', products.create);
    // RETRIEVE
    app.get('/products', products.list);
    app.get('/product/:id', products.detail);
    // UPDATE
    app.put('/product/:id', products.update);
    // DELETE
    app.delete('/product/:id', products.delete);


    /*  Car routes
     ---------------
     We create a variable "relations" that holds the controller object.
     We map the URL to a method in the created variable "relations".
     In this example is a mapping for every CRUD action.
     */
    var cars = require('../app/controllers/cars.js');
    // CREATE
    app.post('/car/', cars.create);
    // RETRIEVE
    app.get('/cars', cars.list);
    app.get('/cars/:id', cars.detail);
    // UPDATE
    app.put('/car/:id', cars.update);
    // DELETE
    app.delete('/car/:id', cars.delete);



    /*  Transaction routes
     ---------------
     We create a variable "relations" that holds the controller object.
     We map the URL to a method in the created variable "relations".
     In this example is a mapping for every CRUD action.
     */
    var transactions = require('../app/controllers/transactions.js');
    // CREATE
    app.post('/transaction', transactions.create);
    // RETRIEVE
    app.get('/transactions', transactions.list);
    app.get('/transaction/:id', transactions.detail);
    app.get('/transactions/bidsPerCar', transactions.bidsPerCar);
    // UPDATE
    app.put('/transaction/:id', transactions.update);
    // DELETE
    app.delete('/transaction/:id', transactions.delete);

}